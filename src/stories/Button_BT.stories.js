import BTButton from './Button_BT.vue';

export default {
    title: 'UI Elements/Button',
    component: BTButton,
};

const Template = (args) => ({
    components: { BTButton },
    setup() {
        return { args };
    },
    template: '<BTButton v-bind="args" />',
});


export const Success = Template.bind({})
Success.args = {
    label: "Go back to Main Page",
    success: true
}

export const Fail = Template.bind({})
Fail.args = {
    label: "Try another code",
    success: false
}