import BTCheckbox from './Checkbox.vue';

export default{
    title: 'UI Elements/Checkbox',
    component: BTCheckbox
}

const Template = (args) => ({
    components: {BTCheckbox},
    setup(){
        return {args};
    },
    template: '<BTCheckbox v-bind="args" />'
});

export const Default = Template.bind({});
Default.args = {
    
}