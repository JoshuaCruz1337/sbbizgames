import BTCard from './Card.vue';

export default{
    title: 'UI Elements/Card',
    component: BTCard
}

const Template = (args) => ({
    components: {BTCard},
    setup(){
        return {args};
    },
    template: '<BTCard v-bind="args" />'
});

export const Default = Template.bind({});
Default.args = {
    label: "Enter the code you got from the coupon",
    success: true
}

export const Invalid = Template.bind({});
Invalid.args = {
    label: "Enter the code you got from the coupon",
    successNum: false,
    successCod: false
}
