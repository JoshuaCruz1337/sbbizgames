import BTField from './Field.vue';

export default{
    title: 'UI Elements/Field',
    component: BTField
}

const Template = (args) => ({
    components: {BTField},
    setup(){
        return {args};
    },
    template: '<BTField v-bind="args" />'
});

export const Default = Template.bind({});
Default.args = {
    placeholder: "Username",
    valid: true
}

export const Valid = Template.bind({});
Valid.args = {
    placeholder: "Username",
    value: "Daniel",
    valid: true
}

export const Invalid = Template.bind({});
Invalid.args = {
    placeholder: "Username",
    value: "D@an!3l",
    valid: false
}